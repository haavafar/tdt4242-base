describe('Functionality', () => {
    before(() => {
        // Login as admin using custom function defined in /support/commands.js
        cy.login()
        cy.django_login()
    })
    beforeEach(() => {
        // We want to keep our tokens (from the login) while running each test
        // Preserving it here means that any alterations of cookies in a test will be self-contained
        Cypress.Cookies.preserveOnce("access", "refresh", "sessionid")
    })

    // Simulates a mock facility's lifecycle
    // Does creation, front-end validation, and deletion
    it('facility lifecycle', () => {
        // Using data from the following JSON file
        cy.fixture("auth.json").then((users)=> {

            // Creates a facility using preset data
            cy.visit('admin/facilities/facility/add/')
            cy.get('#facility_form').within(($form) => {
                cy.get('input[name="name"]').type(users.testfacility.name)
                cy.get('input[name="date"]').type(users.testfacility.date)
                cy.get('textarea[name="description"]').type(users.testfacility.description)
                cy.get('input[name="latitude"]').type(users.testfacility.latitude)
                cy.get('input[name="longitude"]').type(users.testfacility.longitude)
                cy.get('input[name="image1"]').type(users.testfacility.url)
                cy.get('input[value=Save]').click()
            })

            // Get the new facility's id from the table
            cy.visit('/admin/facilities/facility/').then(() => {
                cy.get('#changelist-form').within(($listform) => {
                    cy.get('a').contains(users.testfacility.name).then((item) => {
                        // Get the id so we can visit the facility in the proper webpage
                        const id = item[0].href.split('/change/')[0].split('/facility/')[1]
                        cy.visit('/facility.html?id=' + id)
                    })
                })
            })

            // Viewing the facility as a customer
            cy.location().should((loc) => {
                expect(loc.pathname.includes('facility.html?id='))
            })
            // Check that name corresponds to inserted name
            cy.get('h3').should('exist')
            cy.get('h3').invoke('text').should('eq', users.testfacility.name)

            // Going back to the django admin page, and cleaning up by deleting the facility
            cy.visit('/admin/facilities/facility/').then(() => {
                cy.get('#changelist-form').within(($listform) => {
                    // Go to the change page for the new facility
                    cy.get('a').contains(users.testfacility.name).click()

                    // Go to the deletion url by swapping /change/ with /delete/
                    cy.url().should('include', '/change/')
                    cy.url().then((url) => {
                        cy.visit(url.split('change')[0] + 'delete/')
                    })
                })
            })
            // Confirming the deletion
            cy.get('input[type="submit"]').should('exist').click()
        })
    })
})