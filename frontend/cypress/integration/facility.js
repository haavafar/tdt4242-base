describe('Facility', () => {
    before(() => {
        // Login as admin using custom function defined in /support/commands.js
        cy.login()
    })
    beforeEach(() => {
        // We want to keep our tokens (from the login) while running each test
        // Preserving it here means that any alterations of cookies in a test will be self-contained
        Cypress.Cookies.preserveOnce("access", "refresh")
    })

    // Checks the frontend loading of facilities depending on whether the facilities table is populated
    it('view facility', () => {
        cy.visit('facilities.html')
        cy.get('#paging-container').children().first().then((cont) => {
            if(cont.text().includes("There are no facilities")){
                // There should not be any facilities displayed, as the message tells us that none exist
                cy.get('#div-content').should('be.empty')
            }else{
                // If we get no error message, we should be able to access the first facility on the page
                cy.get('#div-content').then((content)=>{
                    // Get the first id (this is the facility we will be accessing)
                    const id = content.children().first().attr('href').split("=")[1]

                    // Setup listener for the facility api call
                    cy.intercept(`/api/facilities/${id}/`).as('facility')

                    // Click on the facility to view info
                    cy.get('#div-content').children().first().click()

                    // Validate that the server responds with 200 OK
                    cy.wait('@facility').its('response.statusCode').should('eq',200)

                    cy.get('#img-container').children().should('exist')
                    cy.get('p').should('exist')
                    cy.get('#map').children().should('exist')
                })
            }
        })
    })
})