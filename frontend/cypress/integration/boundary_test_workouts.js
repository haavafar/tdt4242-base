describe("Boundary testing for new workouts", () => {
  beforeEach(() => {
    cy.login();
    Cypress.Cookies.preserveOnce("access", "refresh");
  });

  it("Valid creation of new workout", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("Test Workout");
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid creation of new workout with only mandatory fields", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("Test Workout");
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid workout creation with two exercises ", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("Test Workout");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputNotes").type('Test')

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get('#btn-add-exercise').click({force:true})

    cy.get(".div-exercise-container").eq(1).within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("10", { force: true });
        cy.get('input[name="number"]').type("10", { force: true });
      });

    cy.get("#btn-ok-workout").click();
    cy.url().should('include', '/workouts.html');
  });

  it("Log workout with blank fields", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workout.html");
  });

  it("Log workout with no name", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });
    cy.get("#btn-ok-workout").click();
    cy.contains('This field may not be blank');
  });

  it("Log workout with min name", () => {
    cy.get("#btn-create-workout").click();
    cy.get("#inputName").type("x");

    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });
    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Log workout with too long name", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("x".repeat(101));
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");
    cy.scrollTo("bottom");
    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.contains("Ensure this field has no more than 100 characters");
  });

  it("Log workout with max length name", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("x".repeat(100));
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Log workout with less than max length name", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("x".repeat(99));
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Log workout max date", () => {
    cy.get("#btn-create-workout").click();
    cy.get("#inputName").type("Test Workout");

    cy.get("#inputDateTime").type("9999-12-31T23:59");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });
    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Log workout with less than max date", () => {
    cy.get("#btn-create-workout").click();
    cy.get("#inputName").type("Test Workout");

    cy.get("#inputDateTime").type("9998-11-30T22:59");
        cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });
    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Log workout min date", () => {
    cy.get("#btn-create-workout").click();
    cy.get("#inputName").type("Test Workout");

    cy.get("#inputDateTime").type("0002-01-01T00:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });
    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Log workout less than min date", () => {
    cy.get("#btn-create-workout").click();
    cy.get("#inputName").type("Test Workout");

    cy.get("#inputDateTime").type("0001-01-01T00:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("Notes");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });
    cy.get("#btn-ok-workout").click();
    cy.contains('Datetime has wrong format');
  });

  it("Log workout with no note", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("Test Workout");
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes")

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.contains('This field may not be blank')
  });

  it("Log workout with min length of note", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("Test Workout");
    cy.get("#inputDateTime").type("2021-03-30T13:00");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type(".");

    const filePath = "./TestFile.txt";
    cy.get("#customFile").attachFile(filePath);
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should('include', '/workouts.html');
  });
});
