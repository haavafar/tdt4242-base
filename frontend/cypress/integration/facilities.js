describe('Facilities', () => {
    before (() => {
        // Login as admin using custom function defined in /support/commands.js
       cy.login()
    })
    beforeEach (() => {
        // We want to keep our tokens (from the login) while running each test
        // Preserving it here means that any alterations of cookies in a test will be self-contained
        Cypress.Cookies.preserveOnce("access","refresh")
    })

    // Checks the frontend loading of facilities depending on whether the facilities table is populated
    it('displays facilities', () => {
        cy.visit('facilities.html')
        cy.get('#paging-container').children().first().then((cont) => {
            if(cont.text().includes("There are no facilities")){
                // There should not be any facilities displayed, as the message tells us that none exist
                cy.get('#div-content').should('be.empty')
            }else{
                // If we get no error message, there should be at least one facility displayed on our page
                cy.get('#div-content a').should('have.class','facility')
            }
        })
    })

    // Checks if the button for "next page" works (when applicable)
    it('next page', () => {
        cy.visit('facilities.html?page=1')
        cy.get('#paging-container').children().first().then((cont) => {
            if(cont.text().includes("There are no facilities")){
                // There should not be any facilities displayed, as the message tells us that none exist
                cy.get('#div-content').should('be.empty')
            }
            else{
                cy.get('.pagination').then(($p) => {
                    if($p.html().includes('li')){
                        // If there is a next page, we should be able to go to it (only happens if the amount of facilities has exceeded 5)
                        cy.get('.pagination').find('li').first().click()
                    }
                    else{
                        // If there are less than 5 facilities, yet we alter params to get to the second page, an error message should occur
                        cy.visit('facilities.html?page=2')
                        expect(cy.get('.alert').contains('Could not retrieve facilities!'))
                        expect(cy.get('.alert').contains('Invalid page'))
                    }
                })
            }
        })
    })

    // Checks if the button for "prev page" works (when applicable)
    it('prev page', () => {
        cy.visit('facilities.html?page=2').wait(100)
        cy.get('#div-content').then((cont) => {
            if(cont.html().includes('h5')){
                // If there is a facility on page 2, we should be able to go to the previous page
                cy.get('.pagination').find('li').first().next().click()
            }
            else{
                // If there is no facility on page 2, we should get an error message
                expect(cy.get('.alert').contains('Could not retrieve facilities!'))
                expect(cy.get('.alert').contains('Invalid page'))
            }
        })
    })

    // Checks whether we can access facilities in the Django REST Api service
    it('facilities api', () => {
        cy.getToken().then( (token) => {
            const auth = `Bearer ${ token }`;
            const options = {method: 'GET', url: '/api/facilities/', headers: {"Authorization": auth,}};
            cy.request(options)
        })
    })

    // Checks whether the navbar works correctly with the addition of the facilities tab
    it('navbar',() => {
        // cy.visit only accepts response codes 2XX and 3XX
        cy.visit('index.html')

        cy.get('#nav-facilities').should('not.have.class','active')
        cy.get('#nav-index').should('have.class','active')

        cy.get('#nav-facilities').should('have.attr','href').then((href) => {
            cy.visit(href)
        })

        cy.get('#nav-facilities').should('have.class','active')
        cy.get('#nav-index').should('not.have.class','active')
    })
})
