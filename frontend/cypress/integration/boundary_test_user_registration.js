describe("Register new user", () => {
  it("Register with valid input", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("email@mail.me");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Register with only mandetory valid input", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Register with blank fields", () => {
    cy.register();
    cy.get("#btn-create-account").click();
    cy.get(".alert").contains("Registration failed!");
  });

  it("Register new user with less than max lenth username", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      let max_length_username = Date.now() + "x".repeat(136);
      cy.get('input[name="username"]').type(max_length_username);
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Register new user with max length username", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      let max_length_username = Date.now() + "x".repeat(137);
      cy.get('input[name="username"]').type(max_length_username);
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Testing for too long username", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("x".repeat(151));
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });

  it("Testing for no username", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });

  it("Testing for no password", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser");
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });

  it("Testing for no password1", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser");
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });

  it("Register with invalid email", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("123456789");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="phone_number"]').type("12345678");
      cy.get('input[name="country"]').type("Testonia");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.get(".alert").contains("Registration failed!");
  });

  it("Valid with one more than min length country", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="country"]').type("x");
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });
  
  it("Valid less than max length country", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="country"]').type("x".repeat(49));
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid max length country", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="country"]').type("x".repeat(50));
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Too long country", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="country"]').type("x".repeat(51));
      cy.get('input[name="city"]').type("Testheim");
      cy.get('input[name="street_address"]').type("Testing street");
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });


  it("Valid more than min length of street", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid less than max length of street", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x".repeat(49));
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid Max length street", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x".repeat(50));
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Too long street", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x".repeat(51));
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });

  it("Valid more than min phone number", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x");
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid less than max phone number", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x".repeat(49));
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Valid max length phone number", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x".repeat(50));
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Too long phone number", () => {
    cy.register();
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("testUser" + Date.now());
      cy.get('input[name="email"]').type("test@mail.it");
      cy.get('input[name="password"]').type("123456789");
      cy.get('input[name="password1"]').type("123456789");
      cy.get('input[name="street_address"]').type("x".repeat(51));
    });
    cy.get("#btn-create-account").click();
    cy.contains("Registration failed!");
  });
});
