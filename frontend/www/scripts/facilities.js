async function fetchFacilities(request) {
    // Fetch the page parameter, if it exists. If it doesn't, or isn't an integer, default to page 1
    let url = new URL(window.location);
    let page = url.searchParams.get("page");
    if (typeof page == "string") {
        page = page >>> 0 === parseFloat(page) ? parseInt(page) : 1;
    } else {
        page = 1;
    }

    let response = await sendRequest("GET", `${HOST}/api/facilities/?page=${page}`);

    if (response.ok) {
        let data = await response.json();
        let facilities = data.results;

        // Setup paging menu
        let pagingContainer = document.getElementById('paging-container');
        pagingContainer.appendChild(getPagingMenu(data.count, data.next, data.previous));

        // Setup facilities
        let container = document.getElementById('div-content');
        let facilityTemplate = document.querySelector("#template-facility");
        facilities.forEach(facility => {
            const facilityAnchor = facilityTemplate.content.firstElementChild.cloneNode(true);
            facilityAnchor.href = `facility.html?id=${facility.id}`;

            const h5 = facilityAnchor.querySelector("h5");
            h5.textContent = facility.name;

            const img = facilityAnchor.querySelector("img");
            img.src = facility.image1;

            container.appendChild(facilityAnchor);
        });
    }

    return response;
}

function getPagingMenu(count, nextLink, prevLink){
    let menu = '';
    if(count===0){
        return wrapElement("There are no facilities","p");
    }
    if(prevLink!=null){
        menu += getPagingItem("1",'&laquo;');
        menu += getPagingItem(prevLink.split("page=")[1], "Previous");
    }
    if(nextLink!=null){
        menu += getPagingItem(nextLink.split("page=")[1],"Next");
        menu += getPagingItem(Math.ceil(count/5), '&raquo;');
    }
    let elem = document.createElement('div');
    elem.innerHTML = wrapElement(menu,'ul','pagination');
    return elem;
}

function wrapElement(element, tag, className=""){
    if(className.length > 0){
        className = ' class="'+className+'"'
    }
    return '<'+tag +className+'>'+element+'</'+tag+'>';
}

function getPagingItem(parameter, inner){
    return `<li class="page-item"><a class="page-link" href="?page=${parameter}">${inner}</a></li>`;
}

window.addEventListener("DOMContentLoaded", async () => {
    let response = await fetchFacilities();

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve facilities!", data);
        document.body.prepend(alert);
    }
});
