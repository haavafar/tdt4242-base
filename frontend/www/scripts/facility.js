let map;

async function retrieveFacility(id) {
    let response = await sendRequest("GET", `${HOST}/api/facilities/${id}/`);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve facility data!", data);
        document.body.prepend(alert);
    } else {
        let facilityData = await response.json();

        const h3 = document.querySelector("h3");
        h3.textContent = facilityData.name;

        let p = document.querySelector("#facility-description");
        p.innerHTML = facilityData.description;
 
        let lat = parseFloat(facilityData.latitude);
        let long = parseFloat(facilityData.longitude);

        map = L.map('map').setView([lat, long], 17);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1Ijoib2xhdmhkIiwiYSI6ImNrZjhmdGo5MDAzbHYycWxkeGJncXMzdWgifQ.lsLmKIer8wyqdvtrnOzC0Q'
        }).addTo(map);
        L.marker([lat, long]).addTo(map);

        addImg(facilityData.image1);
        addImg(facilityData.image2);
        addImg(facilityData.image3);
        addImg(facilityData.image4);
        addImg(facilityData.image5);

        let img = document.querySelector("img");
        img.src = facilityData.image1;
    }
}

// Image adding function
function addImg(data){
    if(data != null){
        let img = document.createElement('img');
        img.src = data;
        img.className += "rounded img-fluid img-thumbnail";
        img.style.maxHeight = '100%';
        img.style.maxWidth = '20%';
        document.getElementById("img-container").appendChild(img);
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    const urlParams = new URLSearchParams(window.location.search);

    // view a specific facility
    if (urlParams.has('id')) {
        const facilityId = urlParams.get('id');
        await retrieveFacility(facilityId);
    } else{
        window.location.href=`${HOST}/facilities.html`;
    }
});