from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import mixins, generics
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import permissions
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly,
)
from workouts.mixins import CreateListModelMixin
from workouts.permissions import IsOwner, IsReadOnly
from users.serializers import (
    UserSerializer,
    OfferSerializer,
    AthleteFileSerializer,
    UserPutSerializer,
    UserGetSerializer,
)
from users.models import Offer, AthleteFile
from users.permissions import IsCurrentUser, IsAthlete, IsCoach


class UserList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = UserSerializer
    users = []
    admins = []

    def get(self, request, *args, **kwargs):
        self.serializer_class = UserGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        query_set = get_user_model().objects.all()

        if self.request.user:
            # Return the currently logged in user
            status = self.request.query_params.get("user", None)
            if status and status == "current":
                query_set = get_user_model().objects.filter(pk=self.request.user.pk)

        return query_set


class UserDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView
    ):

    lookup_field_options = ["pk", "username"]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        self.serializer_class = UserPutSerializer
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class OfferList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        generics.GenericAPIView
    ):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @staticmethod
    def filter_queryset_on_status(query_set, status):
        if status is not None:
            query_set = query_set.filter(status=status)
        return query_set

    @staticmethod
    def filter_queryset_on_category(query_set, category, user):
        if category is not None:
            if category == "sent":
                query_set = query_set.filter(owner=user)
            elif category == "received":
                query_set = query_set.filter(recipient=user)
        return query_set

    def get_queryset(self):
        result = Offer.objects.none()
        user = self.request.user
        query_params = self.request.query_params
        status = query_params.get("status", None)
        category = query_params.get("category", None)

        if user:
            query_set = Offer.objects.filter(
                Q(owner=user) | Q(recipient=user)
            ).distinct()
            query_set = self.filter_queryset_on_status(query_set, status)
            query_set = self.filter_queryset_on_category(query_set, category, user)
            return query_set

        return result


class OfferDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
    ):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class AthleteFileList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        CreateListModelMixin,
        generics.GenericAPIView,
    ):
    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsCoach)]
    parser_classes = [MultiPartParser, FormParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        query_set = AthleteFile.objects.none()

        if self.request.user:
            query_set = AthleteFile.objects.filter(
                Q(athlete=self.request.user) | Q(owner=self.request.user)
            ).distinct()

        return query_set


class AthleteFileDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
    ):
    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsOwner)]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
