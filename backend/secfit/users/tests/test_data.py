
test_data = {
    "username": "testCreate",
    "email": "test@test.com",
    "password": "1",
    "password1": "1",
    "phone_number": "12345678",
    "country": "Norway",
    "city": "Oslo",
    "street_address": "address 10"
}

test_data1 = {
    "username": "testCreate",
    "email": "test@test.com",
    "password": "",
    "password1": "",
    "phone_number": "12345678",
    "country": "Norway",
    "city": "Oslo",
    "street_address": "address 10"
}

variables = [
    'username',
    'email',
    'password',
    'password1',
    'phone_number',
    'country',
    'city',
    'street_address'
]
variable_values = {
    'valid':
        {
            'username': 'Testuser',
            'email': 'test@test.te',
            'password': 'password',
            'password1': 'password',
            'phone_number': '1234567890',
            'country': 'Testonia',
            'city': 'Testheim',
            'street_address': 'Test',
        },
    'invalid': {
        'username': 'Test user',
        'email': 'test',
        'password': None,
        'password1': None,
        'phone_number': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        'country': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        'city': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        'street_address': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    },
    'blank': {
        'username': '',
        'email': '',
        'password': '',
        'password1': '',
        'phone_number': '',
        'country': '',
        'city': '',
        'street_address': '',
    }
}

test_cases = [
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "invalid",
            "country": "valid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "invalid",
            "country": "blank",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "blank",
            "country": "valid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "valid",
            "phone_number": "valid",
            "country": "invalid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "valid",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "invalid",
            "country": "invalid",
            "city": "blank",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "valid",
            "country": "valid",
            "city": "invalid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "blank",
            "city": "valid",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "blank",
            "street_address": "blank"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "invalid",
            "city": "valid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "blank",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "invalid",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "invalid",
            "country": "valid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "valid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "valid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "blank",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "invalid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "blank",
            "country": "blank",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "valid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "invalid",
            "city": "blank",
            "street_address": "blank"
        }
]
