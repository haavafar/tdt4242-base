import uuid
from django.test import TestCase, Client
from users.tests.test_data import variable_values, test_cases


class UserRegistration2WayTestCase(TestCase):

    def setUp(self):
        print("Running two way user registration")
        self.client = Client()

    def test_two_way(self):
        """
        Performing 2-way domain test
        """
        for testcase in test_cases:
            request = {}
            invalid_request = False
            for key, value in testcase.items():
                request[key] = variable_values[value][key]

                if key == 'username' and value == 'valid':
                    # Add a uid to username to avoid "user already exists error"
                    request['username'] = request['username'] + str(uuid.uuid4())
                if value == 'invalid':
                    invalid_request = True
                elif key == ('username' or 'password' or 'password1') and value == 'blank':
                    invalid_request = True

            # Checking if the repsonse should return status code 400 or 201
            response_code = 400 if invalid_request else 201
            response = self.client.post('/api/users/', request)
            self.assertEqual(response.status_code, response_code)
