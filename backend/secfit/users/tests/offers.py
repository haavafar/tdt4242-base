from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from users.models import Offer


class OfferTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user_model = get_user_model()
        self.password = "password"
        self.user1 = self.user_model(
            username="testOffer1",
            email="test@test.com",
            phone_number="12345678",
            country="Norway",
            city="Oslo",
            street_address="address 10"
        )
        self.user1.set_password(self.password)
        self.user1.save()

        self.user2 = self.user_model(
            username="testOffer2",
            email="test@test.com",
            phone_number="12345678",
            country="Norway",
            city="Oslo",
            street_address="address 10"
        )
        self.user2.set_password(self.password)
        self.user2.save()

    def _send_offer(self, user_id, status):
        request = {"status": status, "recipient": "http://127.0.0.1:8000/api/users/{0}/".format(user_id)}
        response = self.client.post('/api/offers/', request)
        return response.status_code

    def _login(self, user):
        request = {"username": user.username, "password": self.password}
        response = self.client.post('/api/token/', request)
        access_token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + access_token)

    def test_create_offer(self):
        recipient = self.user2.id
        self._login(self.user1)
        status_code = self._send_offer(recipient, Offer.PENDING)
        self.assertEqual(status_code, 201)

    def test_get_offers(self):
        self._login(self.user1)
        self._send_offer(self.user2.id, Offer.PENDING)
        self._login(self.user2)
        response = self.client.get('/api/offers/')
        number_of_offers = response.data["count"]
        self.assertEqual(1, number_of_offers)

    def test_get_offer_detail(self):
        self._login(self.user1)
        self._send_offer(self.user2.id, Offer.PENDING)
        self._login(self.user2)
        response = self.client.get('/api/offers/1/')
        self.assertEqual(200, response.status_code)

    def test_decline_offer(self):
        self._login(self.user1)
        self._send_offer(self.user2.id, Offer.PENDING)
        self._login(self.user2)
        response = self.client.patch('/api/offers/1/', {"status": Offer.DECLINED})
        self.assertEqual(200, response.status_code)

    def test_accept_offer(self):
        self._login(self.user1)
        self._send_offer(self.user2.id, Offer.PENDING)
        self._login(self.user2)
        offer_response = self.client.patch('/api/offers/1/', {"status": Offer.ACCEPTED})
        self.assertEqual(200, offer_response.status_code)

        coach_response = self.client.patch(
            '/api/users/{0}/'.format(self.user2.id),
            {"coach": "http://127.0.0.1:8000/api/users/{0}/".format(self.user1.id)}
        )
        self.assertEqual(200, coach_response.status_code)

        athlete = self.user_model.objects.get(username=self.user2.username)
        self.assertEqual(athlete.coach.id, self.user1.id)
