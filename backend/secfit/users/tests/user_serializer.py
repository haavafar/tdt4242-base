from django.test import TestCase
from django.contrib.auth import get_user_model
from users.serializers import UserSerializer
from users.tests.test_data import test_data, test_data1


class UserSerializerTestCase(TestCase):

    def setUp(self):
        self.user_model = get_user_model()
        user = self.user_model(
            username="test",
            email="test@test.com",
            phone_number="12345678",
            country="Norway",
            city="Oslo",
            street_address="address 10"
        )
        password = "password"
        user.set_password(password)
        user.save()
        self.data = test_data
        self.data1 = test_data1

    def test_create(self):
        user_create = UserSerializer.create(UserSerializer(), validated_data=self.data)
        self.assertEqual(self.user_model.objects.get(username="testCreate"), user_create)

    def test_validate_password(self):
        password = "password"
        response = UserSerializer(data=self.data1).validate_password(value=password)
        self.assertEqual(response, password)
