from django.test import TestCase
from django.contrib.auth import get_user_model
import datetime
from workouts.serializers import WorkoutSerializer, ExerciseSerializer
from workouts.models import Workout, Exercise, ExerciseInstance
from workouts.tests.test_data import test_data, test_data1, test_exercise


class WorkoutSerializerTestCase(TestCase):
    """WorkoutSerializer test class"""

    def setUp(self):
        self.owner_model = get_user_model()
        owner = self.owner_model(
            username="test",
            email="test@test.com",
            phone_number="12345678",
            country="Norway",
            city="Oslo",
            street_address="address 10"
        )
        password = "password"
        owner.set_password(password)
        owner.save()
        self.workout = Workout(
            name="testCreate",
            date="2021-03-20 19:00+0100",
            owner=owner,
            owner_id="1",
            pk=1,
        )
        self.exercise = Exercise(
            name="walk",
            description="hard",
            unit="1",
        )
        self.instance = ExerciseInstance(
            workout=self.workout,
            exercise=self.exercise,
            sets=10,
            number=10,
            id=1,
            exercise_id=1,
        )

    def test_create(self):
        workout_create = WorkoutSerializer.create(WorkoutSerializer(), validated_data=test_data)
        self.assertEqual(workout_create, self.workout)

    def test_update(self):
        """
        Compare mocked exercise and workout to serialized versions respectively

        """

        e = test_data1.copy()
        e["exercise_instances"] = {0: {
            "sets": "10",
            "number": "10",
            "exercise_id": "1",
        }}
        ExerciseSerializer.create(ExerciseSerializer(), validated_data=test_exercise)
        workout_create = WorkoutSerializer.create(WorkoutSerializer(), validated_data=test_data1.copy())

        # Adds the exercise instance to the workout
        workout_updated = WorkoutSerializer.update(
            WorkoutSerializer(),
            instance=workout_create,
            validated_data=e
        )

        # Remove state identifier from the objects
        delattr(workout_updated, "_state")
        delattr(self.workout, "_state")
        real_exercise = ExerciseInstance.objects.all()[0]
        delattr(real_exercise, "_state")
        mock_exercise = self.instance
        delattr(mock_exercise, "_state")

        # Compare the fields in the objects directly using vars()
        self.assertEqual(vars(workout_updated), vars(self.workout))
        self.assertEqual(vars(real_exercise), vars(mock_exercise))
