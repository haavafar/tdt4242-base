"""
Tests for the workouts application.
"""
import datetime
from rest_framework import permissions
import pytz
from django.test import TestCase
from workouts.permissions import \
    IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, \
    IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, \
    IsReadOnly
from workouts.models import Workout
from users.models import User


class MockRequest:
    """
    Imitates a (mutable) request
    """
    def __init__(self):
        self.method = ""
        self.data = ""
        self.user = None


class MockWorkout:
    """
    Imitates a workout
    """
    def __init__(self):
        try:
            user = User.objects.get(pk='1')
        except User.DoesNotExist:
            user = User.objects.create()

        workout_data = {
            "name": "Test Workout",
            "date": datetime.datetime(2021, 2, 19, 9, 33, 0, 0, pytz.UTC),
            "notes": "notes",
            "visibility": "PU",
            "owner": user
        }
        self.workout = Workout.objects.create(**workout_data)
        self.workout.owner.coach = User()


class IsOwnerTestCase(TestCase):
    """
    Ownership permissions
    """
    def setUp(self):
        self.is_owner = IsOwner()
        self.request = MockRequest()
        self.workout = MockWorkout()
        self.request.user = User()

    def test_has_object_permission(self):
        """
        Object permission
        """
        self.assertIs(self.is_owner.has_object_permission(
            self.request, None, self.workout.workout
        ), False)
        self.request.user = self.workout.workout.owner
        self.assertIs(self.is_owner.has_object_permission(
            self.request, None, self.workout.workout
        ), True)


class IsOwnerOfWorkoutTestCase(TestCase):
    """
    Owner of workout permissions
    """
    def setUp(self):
        self.is_owner_of_workout = IsOwnerOfWorkout()
        self.request = MockRequest()
        self.workout = MockWorkout()

    def test_has_permission_method(self):
        """
        GET permission
        """
        request = MockRequest()
        request.method = "GET"
        self.assertIs(self.is_owner_of_workout.has_permission(request, None), True)

    def test_has_permission_workout(self):
        """
        POST workout permission
        """
        request = MockRequest()
        request.method = "POST"
        request.data = {"workout": ""}
        self.assertIs(self.is_owner_of_workout.has_permission(request, None), False)

    def test_has_permission_user(self):
        """
        POST workout permission via REST API
        """
        request = MockRequest()
        request.method = "POST"
        request.user = self.workout.workout.owner
        request.data = {
            "workout": "http://localhost:8000/api/workouts/1/"
        }
        self.assertIs(self.is_owner_of_workout.has_permission(
            request, None
        ), True)

    def test_has_object_permission(self):
        """
        Ownership permissions
        """
        self.assertIs(self.is_owner_of_workout.has_object_permission(
            self.request, None, self.workout
        ), False)
        self.request.user = self.workout.workout.owner
        self.assertIs(self.is_owner_of_workout.has_object_permission(
            self.request, None, self.workout
        ), True)


class IsCoachAndVisibleToCoachTestCase(TestCase):
    """
    Coach permissions
    """
    def setUp(self):
        self.is_coach_and_visible_to_coach = IsCoachAndVisibleToCoach()
        self.request = MockRequest()
        self.request.user = User()
        self.workout = MockWorkout()

    def test_has_object_permission(self):
        """
        Coach has object permissions
        """
        self.assertIs(self.is_coach_and_visible_to_coach.has_object_permission(
            self.request,
            None,
            self.workout.workout
        ), False)

        self.workout.workout.owner.coach = self.request.user
        self.assertIs(self.is_coach_and_visible_to_coach.has_object_permission(
            self.request,
            None,
            self.workout.workout
        ), True)


class IsCoachOfWorkoutAndVisibleToCoachTestCase(TestCase):
    """
    Coach permissions for a specific workout
    """
    def setUp(self):
        self.is_coach_of_workout_and_visible_to_coach = IsCoachOfWorkoutAndVisibleToCoach()
        self.request = MockRequest()
        self.request.user = User()
        self.workout = MockWorkout()

    def test_has_object_permission(self):
        """
        Coach permissions
        """
        self.assertIs(self.is_coach_of_workout_and_visible_to_coach.has_object_permission(
            self.request,
            None,
            self.workout
        ), False)

        self.workout.workout.owner.coach = self.request.user
        self.assertIs(self.is_coach_of_workout_and_visible_to_coach.has_object_permission(
            self.request,
            None,
            self.workout
        ), True)


class IsPublicTestCase(TestCase):
    """
    Test case public permissions
    """
    def setUp(self):
        self.workout = MockWorkout()
        self.is_public = IsPublic()

    def test_has_object_permission(self):
        """
        Workout permissions
        """
        self.assertIs(self.is_public.has_object_permission(
            None,
            None,
            self.workout.workout
        ), True)

        self.workout.workout.visibility = "CO"
        self.assertIs(self.is_public.has_object_permission(
            None,
            None,
            self.workout.workout
        ), False)


class IsWorkoutPublicTestCase(TestCase):
    """
    Workout is publicly available
    """
    def setUp(self):
        self.workout = MockWorkout()
        self.is_workout_public = IsWorkoutPublic()

    def test_has_object_permission(self):
        """
        Workout permissions
        """
        self.assertIs(self.is_workout_public.has_object_permission(
            None,
            None,
            self.workout
        ), True)

        self.workout.workout.visibility = "N"
        self.assertIs(self.is_workout_public.has_object_permission(
            None,
            None,
            self.workout
        ), False)


class IsReadOnlyTestCase(TestCase):
    """
    Read only permission
    """
    def setUp(self):
        self.is_read_only = IsReadOnly()
        self.request = MockRequest()
        self.request.method = permissions.SAFE_METHODS.__getitem__(1)

    def test_has_object_permission(self):
        """
        Object permissions
        """
        self.assertIs(self.is_read_only.has_object_permission(self.request, None, None), True)

        self.request.method = None
        self.assertIs(self.is_read_only.has_object_permission(self.request, None, None), False)
