"""Serializers for the workouts application
"""
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile, RememberMe


class ExerciseInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an ExerciseInstance. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, exercise, sets, number, workout

    Attributes:
        workout:    The associated workout for this instance, represented by a hyperlink
    """

    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail", required=False
    )

    class Meta:
        model = ExerciseInstance
        fields = ["url", "id", "exercise", "sets", "number", "workout"]


class WorkoutFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a WorkoutFile. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, owner, file, workout

    Attributes:
        owner:      The owner (User) of the WorkoutFile, represented by a username. ReadOnly
        workout:    The associate workout for this WorkoutFile, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail", required=False
    )

    class Meta:
        model = WorkoutFile
        fields = ["url", "id", "owner", "file", "workout"]

    def create(self, validated_data):
        return WorkoutFile.objects.create(**validated_data)


class WorkoutSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Workout. Hyperlinks are used for relationships by default.

    This serializer specifies nested serialization since a workout consists of WorkoutFiles
    and ExerciseInstances.

    Serialized fields: url, id, name, date, notes, owner, owner_username, visiblity,
                       exercise_instances, files

    Attributes:
        owner_username:     Username of the owning User
        exercise_instance:  Serializer for ExericseInstances
        files:              Serializer for WorkoutFiles
    """

    owner_username = serializers.SerializerMethodField()
    exercise_instances = ExerciseInstanceSerializer(many=True, required=True)
    files = WorkoutFileSerializer(many=True, required=False)

    class Meta:
        model = Workout
        fields = [
            "url",
            "id",
            "name",
            "date",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "exercise_instances",
            "files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating ExerciseInstances, WorkoutFiles, and a Workout.

        This is needed to iterate over the files and exercise instances, since this serializer is
        nested.

        Args:
            validated_data: Validated files and exercise_instances

        Returns:
            Workout: A newly created Workout
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        validated_files = []
        if "files" in validated_data:
            validated_files = validated_data.pop("files")

        workout = Workout.objects.create(**validated_data)

        for exercise_instance_data in exercise_instances_data:
            ExerciseInstance.objects.create(workout=workout, **exercise_instance_data)
        for file_data in validated_files:
            WorkoutFile.objects.create(
                workout=workout, owner=workout.owner, file=file_data.get("file")
            )

        return workout

    @staticmethod
    def delete_files(instance, data):
        """"Removes the selected data from the instanced object

        Args:
            instance: Data object
            data: Data to remove
        """
        for i in range(len(data), len(instance.all())):
            instance.all()[i].delete()

    @staticmethod
    def create_workout_files(instance, files, validated_files):
        """Creates the workout files

        Args:
            instance: Workout instance
            files: Collection of files
            validated_files: Data for validated fields

        Returns:
            instance: Workout instance
        """
        for i in range(len(files.all()), len(validated_files)):
            WorkoutFile.objects.create(
                workout=instance,
                owner=instance.owner,
                file=validated_files[i].get("file"),
            )
        return instance

    @staticmethod
    def create_exercise_instance(instance, exercise_instances, exercise_instances_data):
        """Creates the workout files

        Args:
            instance: Workout instance
            exercise_instances: Collection of workouts
            exercise_instances_data: Data for workout fields

        Returns:
            instance: Workout instance
        """
        for i in range(len(exercise_instances.all()), len(exercise_instances_data)):
            exercise_instance_data = exercise_instances_data[i]
            ExerciseInstance.objects.create(
                workout=instance, **exercise_instance_data
            )
        return instance

    @staticmethod
    def update_exercise_instances(exercise_instances, exercise_instances_data):
        """Updates existing exercise instances without adding or deleting object

        Args:
            exercise_instances: Collection of workouts
            exercise_instances_data: Data for workout fields
        """

        # zip() will yield n 2-tuples, where n is
        # min(len(exercise_instance), len(exercise_instance_data))
        for exercise_instance, exercise_instance_data in zip(
                exercise_instances.all(), exercise_instances_data
        ):
            exercise_instance.exercise = exercise_instance_data.get(
                "exercise", exercise_instance.exercise
            )
            exercise_instance.number = exercise_instance_data.get(
                "number", exercise_instance.number
            )
            exercise_instance.sets = exercise_instance_data.get(
                "sets", exercise_instance.sets
            )
            exercise_instance.save()

    def update(self, instance, validated_data):
        """Custom logic for updating a Workout with its ExerciseInstances and Workouts.

        This is needed because each object in both exercise_instances and files must be iterated
        over and handled individually.

        Args:
            instance (Workout): Current Workout object
            validated_data: Contains data for validated fields

        Returns:
            Workout: Updated Workout instance
        """
        exercise_instances_data = validated_data.pop("exercise_instances")
        exercise_instances = instance.exercise_instances

        instance.name = validated_data.get("name", instance.name)
        instance.notes = validated_data.get("notes", instance.notes)
        instance.visibility = validated_data.get("visibility", instance.visibility)
        instance.date = validated_data.get("date", instance.date)
        instance.save()

        # Handle ExerciseInstances
        self.update_exercise_instances(exercise_instances, exercise_instances_data)

        # If new exercise instances have been added to the workout, then create them
        if len(exercise_instances_data) > len(exercise_instances.all()):
            instance = self.create_exercise_instance(instance, exercise_instances, exercise_instances_data)
        # Else if exercise instances have been removed from the workout, then delete them
        elif len(exercise_instances_data) < len(exercise_instances.all()):
            self.delete_files(exercise_instances_data, exercise_instances)

        # Handle WorkoutFiles

        if "files" in validated_data:
            validated_files = validated_data.pop("files")
            files = instance.files

            for file, file_data in zip(files.all(), validated_files):
                file.file = file_data.get("file", file.file)

            # If new files have been added, creating new WorkoutFiles
            if len(validated_files) > len(files.all()):
                instance = self.create_workout_files(instance, files, validated_files)

            # Else if files have been removed, delete WorkoutFiles
            elif len(validated_files) < len(files.all()):
                self.delete_files(files, validated_files)

        return instance

    @staticmethod
    def get_owner_username(obj):
        """Returns the owning user's username

        Args:
            obj (Workout): Current Workout

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an Exercise. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, name, description, unit, instances

    Attributes:
        instances:  Associated exercise instances with this Exercise type. Hyperlinks.
    """

    instances = serializers.HyperlinkedRelatedField(
        many=True, view_name="exerciseinstance-detail", read_only=True
    )

    class Meta:
        model = Exercise
        fields = ["url", "id", "name", "description", "unit", "instances"]


class RememberMeSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an RememberMe. Hyperlinks are used for relationships by default.

    Serialized fields: remember_me

    Attributes:
        remember_me:    Value of cookie used for remember me functionality
    """

    class Meta:
        model = RememberMe
        fields = ["remember_me"]
