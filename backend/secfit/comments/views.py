from rest_framework import generics, mixins
from rest_framework import permissions
from rest_framework.filters import OrderingFilter
from comments.models import Comment, Like
from comments.permissions import IsCommentVisibleToUser
from comments.serializers import CommentSerializer
from comments.serializers import LikeSerializer
from workouts.permissions import IsOwner, IsReadOnly

# Create your views here.
class CommentList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        generics.GenericAPIView
):
    # queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [OrderingFilter]
    ordering_fields = ["timestamp"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """A comment should be visible to the requesting user if any of the following hold:
        - The comment is on a public visibility workout
        - The comment was written by the user
        - The comment is on a coach visibility workout and the user is the workout owner's coach
        - The comment is on a workout owned by the user
        """
        workout_pk = self.kwargs.get("pk")
        queryset = Comment.objects.none()

        if workout_pk:
            queryset = Comment.objects.filter(workout=workout_pk)
        elif self.request.user:
            queryset = Comment.objects.filter(
                IsCommentVisibleToUser.has_object_permission
                ).distinct()

        return queryset

# Details of comment
class CommentDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [
        permissions.IsAuthenticated & IsCommentVisibleToUser & (IsOwner | IsReadOnly)
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# List of likes
class LikeList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        generics.GenericAPIView
):
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Like.objects.filter(owner=self.request.user)


# Details of like
class LikeDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]
    _Detail = []

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
