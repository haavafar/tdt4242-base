from django.core.exceptions import ValidationError


def image_size(image):
    if image.file.size > 3 * 1024 * 1024:
        raise ValidationError('Image size needs to be less than 3MB')


# Restricts the amount of words to 500
def max_words(string):
    if len(string.split(" ")) > 500:
        raise ValidationError('Description can contain a maximum of 500 words')
