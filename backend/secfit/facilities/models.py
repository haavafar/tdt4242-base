"""Contains the models for the facilities Django application. Admins create facilities,
which contain descriptions, images and location.
"""
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from .validators import max_words


def facility_directory_path(instance, filename):
    """Return path for which facility files should be uploaded on the web server

    Args:
        instance (FacilityImage): FacilityImage instance
        filename (str): Name of the file

    Returns:
        str: Path where facility file is stored
    """
    return f"facilities/{instance.name}/{filename}"


class Facility(models.Model):
    """Django model for a facility that admins can add.

    A facility has several attributes, and is not associated
    with other parts of the website (as stated in the requirements by our peer group)

    Attributes:
        name:        Name of the facility
        date:        Date the facility was added
        description: Description of the facility
        latitude:    Latitude of the facility
        longitude:   Longitude of the facility
        image1:      Image of the facility
        image2:      Image of the facility
        image3:      Image of the facility
        image4:      Image of the facility
        image5:      Image of the facility
    """

    name = models.CharField(max_length=50, unique=True)
    date = models.DateField()
    description = models.TextField(validators=[max_words])

    latitude = models.DecimalField(
        decimal_places=5,
        max_digits=7,
        validators=[MaxValueValidator(90), MinValueValidator(-90)]
        )

    longitude = models.DecimalField(
        decimal_places=5,
        max_digits=8,
        validators=[MaxValueValidator(180), MinValueValidator(-180)]
        )

    image1 = models.CharField(max_length=2048)
    image2 = models.CharField(max_length=2048, blank=True)
    image3 = models.CharField(max_length=2048, blank=True)
    image4 = models.CharField(max_length=2048, blank=True)
    image5 = models.CharField(max_length=2048, blank=True)


    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return self.name
