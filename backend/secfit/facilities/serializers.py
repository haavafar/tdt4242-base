from rest_framework import serializers
from .models import Facility


class FacilityPostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Facility
        fields = [
            "id",
            "name",
            "date",
            "description",
            "latitude",
            "longitude",
            "image1",
            "image2",
            "image3",
            "image4",
            "image5",
        ]

    def create(self, validated_data):
        return Facility.objects.create(**validated_data)


class FacilityGetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Facility
        fields = [
            "id",
            "name",
            "date",
            "description",
            "latitude",
            "longitude",
            "image1",
            "image2",
            "image3",
            "image4",
            "image5",
        ]
