"""AppConfig for facilities app
"""
from django.apps import AppConfig


class FacilitiesConfig(AppConfig):
    """AppConfig for workouts app

    Attributes:
        name (str): The name of the application
    """

    name = "facilities"
    