"""Contains views for the workouts application. These are mostly class-based views.
"""
from rest_framework.response import Response
from rest_framework import generics, mixins
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from rest_framework.reverse import reverse
from .models import Facility
from .serializers import FacilityGetSerializer, FacilityPostSerializer


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "facilities": reverse("facility-list", request=request, format=format),
        }
    )


class FacilityGeneration(
        mixins.CreateModelMixin,
        generics.GenericAPIView
):
    """Class defining the web response for creation of a facility

        HTTP methods: POST
        """
    serializer_class = FacilityPostSerializer
    queryset = Facility.objects.all()
    permission_classes = [permissions.IsAdminUser]

    def post(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000
    ordering = '-date'


class FacilityList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        generics.GenericAPIView
):
    """Class defining the web response for retrieval of multiple facilities

    HTTP methods: GET
    """
    serializer_class = FacilityGetSerializer
    queryset = Facility.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    ordering_fields = ["name", "date"]
    pagination_class = StandardResultsSetPagination

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class FacilitySingle(
        mixins.RetrieveModelMixin,
        generics.GenericAPIView,
):
    """Class defining the web response for the details of an individual Facility.

    HTTP methods: GET
    """
    serializer_class = FacilityGetSerializer
    queryset = Facility.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
