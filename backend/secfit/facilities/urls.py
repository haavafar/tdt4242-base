from django.urls import path
from facilities import views

urlpatterns = [
    path("", views.api_root),
    path("api/facilities/", views.FacilityList.as_view(), name="facility-list"),
    path("api/facilities/<int:pk>/", views.FacilitySingle.as_view(), name="facility-detail"),
    path("api/facilities/<str:name>/", views.FacilitySingle.as_view(), name="facility-detail"),
]
