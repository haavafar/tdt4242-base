# SecFit Group 11

SecFit (Secure Fitness) is a hybrid mobile application for fitness logging.

## Excersice 2 - DevOps and Testing

### Task 1

#### Implementation of new features
##### Backend
A database table has been created for facilities. This includes all the documents in `backend/secfit/facilities/`. The data can be accessed through the Django REST Api, or by going to the django admininistrator page.
Some initial test data has also been added to `backend/secfit/seed.json`.
##### Frontend
We have added the page `facilities` to the navbar. This page shows an overview of multiple facilities. Paging has been implemented, so you can see a maximum of five facilities at one time. Paging menu buttons appear when appropriate. 
- HTML file: `frontend/www/facilities.html`
- Associated script: `frontend/www/scripts/facilities.js`

We have also added the page `facility` to the site. This can be accessed by clicking a facility preview from the `facilities` page, or alternatively typing in the url `/facility/?id=x`, where x is the id of the facility.
- HTML file: `frontend/www/facility.html`
- Associated script: `frontend/www/scripts/facility.js`



#### DevOps
The project is set up with Gitlab's CI/CD. It is structured into two stages, testing, and deploy. The project needs to successfully build and get approved by tested in order to deploy. The application is deployed to two Heroku Servers, one for hosting the frontend and the second for the backend. For more information check out `gitlab-ci.yml`

### Task 2
In task 2 we used Django Testcase to create unit test for `./workouts/permissions.py` and for the class UserSerializer in `./users/serializer.py`. Theses are added to CI/CD and has to passed in order to deploy. 

The test files can be found at:
- Permissions: `backend/secfit/workouts/tests.py`
- Serializer: `backend/secfit/users/tests/UserSerializer.py`

### Task 3
In task 3 we practised created black-boxed tests. The tests where created using either DjangoTestCase or Cypress.io. 

The tests can be found at: 
- Boundary Value Tests: 
  - Registration page: `frontend/cypress/integration/boundary_test_user_registration.js`
  - New Workout page : `frontend/cypress/integration/boundar_test_workouts.js`
- 2-way Domain Test: `backend/secfit/users/tests/UserRegistration.py`
- Integration Test: `frontend/cypress/integration/`
- Black box tests for FR5: 

## Run tests
### Setup for local testing
In order to run cypress tests please run createsuperuser and add sample facilites to local database from `seed.json`. 
To do this run:
1. `cd backend/secfit`
2. `python manage.py loaddata seed.json`
3. `python manage.py makemigration`
4. `python manage.py createsuperuser` with username `admin` and password `Password`

### Run Backend tests
- `cd backend/secfit`
- `python manage.py test`

### Run frontend tests
- Build project with `docker-compose up --build` 
- Open a new terminal and navigate to project root
- `cd frontend`
- `npm install`
- `npx cypress run`


## Deploy with Docker

### Prerequisites:

Docker

Git

Windows hosts must use Education or more advanced versions to run Docker \
Download: https://innsida.ntnu.no/wiki/-/wiki/English/Microsoft+Windows+10

### Install:

$ git clone https://gitlab.stud.idi.ntnu.no/kyleo/secfit.git \
$ cd secfit/

### Run:

$ docker-compose up --build \
Hosts the application on http://localhost:9090 with default settings


## Technology
- **deployment** Docker
- **web** Nginx
- **database** Postgre SQL
- **backend** Django 3 with Django REST framework
- **application** 
    - **browser** - HTML5/CSS/JS, Bootstrap v5 (no jQuery dependency)
    - **mobile** Apache Cordova (uses same website)
- **authentication** JWT


## Code and structure

.gitlab-ci.yml - gitlab ci
requirements.txt - Python requirements
package.json - Some node.js requirements, this is needed for cordova

- **secfit/** django project folder containing the project modules
  - **<application_name>/** - generic structure of a django application
    - **admins.py** - file contaning definitions to connect models to the django admin panel
    - **urls.py** - contains mapping between urls and views
    - **models.py** - contains data models
    - **permissions.py** - contains custom permissions that govern access
    - **serializers.py** - contains serializer definitions for sending data between backend and frontend
    - **parsers.py** - contains custom parsers for parsing the body of HTTP requests
    - **tests/** - contains tests for the module. [View Testing in Django](https://docs.djangoproject.com/en/2.1/topics/testing/) for more.
    - **views.py** - Controller in MVC. Methods for rendering and accepting user data
    - **forms.py**  -  definitions of forms. Used to render html forms and verify user input
    - **settings.py** - Contains important settings at the application and/or project level
    - **Procfile** - Procfile for backend heroku deployment
  - **media/** - directory for file uploads (need to commit it for heroku)
  - **comments/** - application handling user comments and reactions
  - **secfit/** - The projects main module containing project-level settings.
  - **users/** - application handling users and requests
  - **workouts/** - application handling exercises and workouts
  - **manage.py** - entry point for running the project.
  - **seed.json** - contains seed data for the project to get it up and running quickly (coming soon)



## Local setup
It's recommended to have a look at: https://www.djangoproject.com/start/
Just as important is the Django REST guide: https://www.django-rest-framework.org/

Create a virtualenv https://docs.python-guide.org/dev/virtualenvs/


### Django

Installation with examples for Ubuntu. Windows and OSX is mostly the same

Fork the project and clone it to your machine.

#### Setup and activation of virtualenv (env that prevents python packages from being installed globaly on the machine)
Naviagate into the project folder, and create your own virtual environment


#### Install python requirements

`pip install -r requirements.txt`


#### Migrate database

`python manage.py migrate`


#### Create superuser

Create a local admin user by entering the following command:

`python manage.py createsuperuser`

Only username and password is required


#### Start the app

`python manage.py runserver`


#### Add initial data

You can add initial data either by going to the url the app is running on locally and adding `/admin` to the url.

Add some categories and you should be all set.

Or by entering 

`python manage.py loaddata seed.json`

### Cordova
Cordova CLI guide: https://cordova.apache.org/docs/en/latest/guide/cli/
If you want to run this as a mobile application
- Navigate to the frontend directory
- For android, do `cordova run android`
- For ios, do `cordova run ios`
- For browser, do `cordova run browser`

It's possible you will need to add the platforms you want to run and build.
The following documentation can be used to run the application in an Android emulator: \
https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html
